\usepackage[left=2cm, right=2cm, top=3.75cm, bottom=4cm]{geometry}
\usepackage{color}
\usepackage{graphicx}
\usepackage{fancyhdr}
\pagestyle{fancy}
\usepackage{ifthen}
 
 
\definecolor{TURQUOISE}{rgb}{0,0.67,0.61}
\definecolor{ROSE}{rgb}{1,0,0.51}
\definecolor{VERT}{rgb}{0.25,1,0.15}
\definecolor{ORANGE}{rgb}{0.99,0.53,0.07}
 
\newcommand{\pageDeGarde}[5][]
{
\thispagestyle{empty}
\begin{picture}(0,0) \put(-74,36){\includegraphics[width=210mm]{Img/logoSupGalileeUP13-une.jpg}}\end{picture}
\begin{picture}(0,0) \put(-250,-250){\includegraphics[width=282.08mm,height=109.mm,angle=180]{Img/logo_erasmus.png}}\end{picture}
\begin{picture}(0,0)\put(267,-728){\includegraphics[width=87.6mm,height=25mm]{footer.png}}
\put(32,-100){\color{ROSE}{\begin{minipage}{13cm} \huge {#2} \end{minipage}}}
\put(-75,-560){\color{VERT}{\begin{minipage}{6.3cm} \large \raggedleft  #3 \end{minipage}}}
\put(150,-560){\color{ORANGE}{\begin{minipage}{20.5cm} \large \raggedright #4\end{minipage}}}
\put(-20,-700){\color{TURQUOISE}{\begin{minipage}{8cm} \large #5\end{minipage}}}
\end{picture}
\newpage
\thispagestyle{empty}
\null
\newpage
}
 
 
\newcommand{\pageMATIERE}[1][]{
\thispagestyle{empty}
\begin{picture}(0,0) \put(-74,36){\includegraphics[width=210mm]{header.png}}\end{picture}
\fancyhead[CO]{}
\fancyhead[RO]{}
\fancyfoot[LO,RO]{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\ifthenelse{\equal{#1}{}}{%
\begin{picture}(0,0)\put(-122,-725){\includegraphics[width=300mm,height=25mm]{volutebas.png}} \end{picture}}%
}
 
 
\newcommand{\pageTHEME}[1][]{
\fancyhead[LO]{\begin{picture}(0,0) \put(-57,-2){\includegraphics[width=210mm]{header.png}}\end{picture}}
\fancyhead[CO]{}
\fancyhead[RO]{}
\fancyfoot[LO,RO]{}
\fancyfoot[RO]{\color{ROSE}{\thepage}}%\put(125,10){\color{ROSE}{\thepage}}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\ifthenelse{\equal{#1}{}}{%
\fancyfoot[CO]{\begin{picture}(0,0)\put(-330,-81){\includegraphics[width=300mm,height=25mm]{volutebas.png}} \end{picture}}%
}
}
