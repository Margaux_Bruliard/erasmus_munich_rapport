\babel@toc {french}{}
\contentsline {section}{Introduction}{2}
\contentsline {section}{\numberline {1}La ville de Munich}{3}
\contentsline {subsection}{\numberline {1.1}La place de Munich dans le paysage allemand}{3}
\contentsline {subsection}{\numberline {1.2}Les lieux et les activit\IeC {\'e}s principales de Munich}{3}
\contentsline {section}{\numberline {2}L'universit\IeC {\'e} Technique de Munich}{5}
\contentsline {section}{\numberline {3}Etre un \IeC {\'e}tudiant international \IeC {\`a} la TUM}{6}
\contentsline {subsection}{\numberline {3.1}Les enseignements}{6}
\contentsline {subsection}{\numberline {3.2}La vie sur le campus en tant qu'\IeC {\'e}tudiant Erasmus}{7}
\contentsline {section}{Conclusion}{8}
